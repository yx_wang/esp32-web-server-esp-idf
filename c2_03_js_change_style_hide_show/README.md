# ESP32-Web-Server编程- JS 基础 4
## 概述

HTML 内联事件处理器，**你永远不应该使用 HTML 事件处理器属性**——因为那些已经过时了，使用它们是不好的做法。

在前端编程中，除了将期望发生的事件写为 JS 文件外，还可以使用一些组件自带的事件处理器。

比如可以使用 button 组件的 onclick 内联属性，实现在网页上点击按钮，切换 LED 灯图标的转变。

但是 HTML 和你的 JavaScript （内联属性）混在一起不是一个好主意，因为它使得整个文件变得难以阅读。

```
<p>
      <button onclick="document.getElementById('text').style.color='red'">Red Text</button>
</p>
```

建议使用独立的函数来控制元素属性，这样容易增加函数的可复用性，并保持 HTML 和 JS 相对独立：

```
 <p>
    <button onclick="bgChange()">Change Background</button>
 </p>
  <script>
    function bgChange() {
      const randomHex = () => `#${Math.floor(Math.random() * 0xffffff).toString(16).padEnd(6, "0")}`;
      document.body.style.backgroundColor = randomHex();
    }
  </script>
```

## 需求及功能解析

本节主要演示 JavaScript （内联属性）的用法，以及通过建立独立的函数来完成同样的功能。读者可以对比两种处理方法。

## 示例解析

### 前端设计

前端代码建立了五个 button，点击不同的 button 可以实现对应的文本颜色的切换。前四个 button 使用 onclick 内联属性。最后一个 button，通过函数 bgChange() 来完成相同的功能。

```
<body>
    <h1>Change text style</h1>
    <p id="text">This text will change style.</p>
    <p>
      <button onclick="document.getElementById('text').style.color='red'">Red Text</button>
    </p>
    <p>
      <button onclick="document.getElementById('text').style.color='blue'">Blue Text</button>
    </p>
    <p>
      <button onclick="document.getElementById('text').style.display='none'">Hide Text</button>
    </p>
    <p>
      <button onclick="document.getElementById('text').style.display='block'">Show Text</button>
    </p>
    <p>
    <button onclick="bgChange()">Change Background</button>
    </p>
    <script>
      function bgChange() {
        const randomHex = () => `#${Math.floor(Math.random() * 0xffffff).toString(16).padEnd(6, "0")}`;
        document.body.style.backgroundColor = randomHex();
      }
    </script><p>
  <img id="imageLamp" src="light_on.png">
</p>
<p>
  <button onclick="document.getElementById('imageLamp').src='light_on.png'">Turn on the light</button>
</p>
<p>
  <button onclick="document.getElementById('imageLamp').src='light_off.png'">Turn off the light</button>
</p>
```

### 示例效果

点击不同的按钮可以切换网页的显示效果：

![](./change_text_style.png)

## 总结

1）本节主要是演示在前端设计中，通过 HTML 内联事件处理器或者  JavaScript 函数实现对应事件的处理。

2）在前端开发中，应尽可能使用  JavaScript 函数，而不是 内联事件处理器，后者在大型项目中维护不方便，容易因为维护问题。

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c2_03_js_change_style_hide_show) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)