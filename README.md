# ESP32-Web-Server-ESP-IDF

## 介绍

使用 [ESP-IDF](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/api-reference/protocols/esp_http_server.html) 在ESP32上构建 HTTP Web 服务器或 WebSocket Web 服务器。

带你玩转物联网小项目开发系列之～玩转 Web server 开发。

## Web 的优势

Web 通过网页提供物联网项目的人机交互界面。

Web 是最便宜的无线“UI”。仅仅通过电脑或者手机上的浏览器就可以立即打开一个人机交互界面。

即便在没有浏览器的平台上，你仍然可以通过一些工具，如 wget、cURL 来从 esp32 web 服务器上下载一些文件或者数据。

## 学习内容

学习使用 ESP32 建立一个 web 访问框架。

学习 HTML、CSS、JavaScript 和 Websocket 和 HTTP协议。

学习一些重要的 web 技术，如SSE（Server-Sent Events）、AJAX（Asynchronous JavaScript and XML）、文件系统。

掌握典型的物联网应用，如通过网页控制物联网设备、通过网页显示传感器的数据、通过网页进行配网、通过网页进行 OTA、登录授权、通过网页传输文件、图片等。

了解技术细节、知其然知其所以然，物联网 DIY 开发人员必备技能。

## 使用说明

1. 基于ESP-IDF v4.4 开发环境。  
2. 主要编程语言为 C 语言，适合嵌入式、物联网开发工程师快速上手。  
3. 分为三个部分：基础篇、提高篇和项目篇。  
4. 涵盖 HTML、CSS、JavaScript 和客户端-服务器通信协议，例如 Websocket 和 HTTP 协议。  
5. 包含一些重要的 web 技术，如SSE（Server-Sent Events）、AJAX（Asynchronous JavaScript and XML）。  
6. 包含一些重要的嵌入式、物联网开发技巧，包括 JSON、字符串封包-解析等使用方法。  
7. 包含一些典型的物联网应用，如通过网页控制物联网设备、通过网页显示传感器的数据、通过网页进行配网、通过网页进行 OTA、登录授权、通过网页传输文件、图片等。  
8. 不同于 [AsyncWeb](https://github.com/me-no-dev/ESPAsyncWebServer)，这里的大部分示例都是基于原生的单线程 [web server](https://docs.espressif.com/projects/esp-idf/zh_CN/release-v4.4/esp32/api-reference/protocols/esp_https_server.html) 进行构建，不考虑并发性问题，后台编程相对简单。  
9. 支持使用两种方式访问网页：  

- sta 模式，即 ESP32 与手机（或电脑）连接同一个路由器或者手机热点，然后通过手机打开网页。(AP 和 Station 的基本介绍参考[AP、STA的概念](https://blog.csdn.net/wangyx1234/article/details/113445041))
- ap 模式，ESP32 作为一个热点，然后手机或者电脑连接 esp32 的特点，然后打开网页。

## 硬件需求

任意一款 ESP32。

## 建立开发环境

参考[官方文档](https://docs.espressif.com/projects/esp-idf/en/release-v4.4/esp32/get-started/index.html)。

## 代码示例

https://gitee.com/yx_wang/esp32-web-server-esp-idf

## 部分项目展示

### 通过网页配网

![](./images/wifi-manage1.png)

### 通过网页进行设备 OTA

![](./images/web_ota2.gif)

### 通过网页控制设备的 GPIO

![](./images/websocket_gpio.gif)

### 通过网页读取并显示 sensor 的数据

![](./images/sensor_reading_charts.png)

### 建立一个登录管理器

![](./images/login-in-page.png)

### 通过网页管理设备的文件

![](./images/file_server.png)

### 通过网页显示图片和视频

![](./images/web_jpeg1.png)

### 通过网页制作告白相册

![](./images/gaobai_show.gif)

### 异步 web 实现多线程 web 服务器

敬请期待，更新中～

## 课程章节

基础篇：熟悉前端编程的基础知识。

进阶篇: 熟悉简单的物联网前后台交互的基本知识。

多媒体篇：了解多媒体图片、视频、文本的快捷传输。

综合项目篇：实际物联网项目中通过网页实现有趣实用的功能。

## 下一篇



#### 参与贡献

1.  Fork [本仓库](https://gitee.com/yx_wang/esp32-web-server-esp-idf)
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

