# ESP32-Web-Server编程- JS 基础 5

## 概述

演示通过 JS 进行温度转换。

![](./temperature_convert.png)

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c2_04_js_celsius_to_fahrenheit) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)
