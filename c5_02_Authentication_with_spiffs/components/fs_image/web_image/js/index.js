$(function(){
	if(!sessionStorage.getItem('login_user')){
		window.location = "./login.html";
	}
	$("#logout").click(function(){
		var xhr = new XMLHttpRequest();
        xhr.open("GET", "./logout", true);
        xhr.send();
        setTimeout(function(){ window.open("/logged-out","_self"); }, 1000);
	})
})

// function logoutButton() {
//     var xhr = new XMLHttpRequest();
//     xhr.open("GET", "/logout", true);
//     xhr.send();
//     setTimeout(function(){ window.open("/logged-out","_self"); }, 1000);
// }