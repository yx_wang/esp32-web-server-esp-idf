$(function(){
	if(!sessionStorage.getItem('login_user')){
		window.location = "./login.html";
	}
	$("#logout").click(function(){
		sessionStorage.clear()
		$.ajax({
			type: "post",
			url: "/logout",
			headers: {
				"Content-Type": "application/json",
			},
			dataType: "json",
			success: function(b) {
				sessionStorage.clear()
				window.location = "./login.html";
			},
			error: function(b) {
				toastr.error(ERROR_INFO[b.error_code]);
				sessionStorage.clear()
				alert('登录失败')
			}
		})
	})
})