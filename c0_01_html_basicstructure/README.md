# ESP32-Web-Server编程-HTML 基础

## 概述

HTML(HyperText Markup Language) 是用来描述网页的一种语言。其相关内容存储在前端代码的 `.html `文件中。

当**浏览器**向 **web 服务器**请求网页时，一个 HTML 文件被发送给浏览器，浏览器解释该文件的内容，呈现具体的图表、文字给用户。

HTML 文档包含了HTML **标签(包含标签和元素，其实他们是一样的)**及**文本内容**，其中

- **HTML 标签**是由**尖括号**包围的关键词，HTML 标签通常是成对出现的，基本格式是 `<标签>内容</标签>`，示例：

  ```
  <h1>这是一个一级标题</h1>
  <h2>这是一个二级标题</h2>
  <p>这是一个段落。</p>
  <a href="https://www.iot-wang.com">这是一个链接</a>
  ```

- 文本内容，标签内的显示字符。

- 还有一些格式化标签（也称为元素）的定义以及注释风格：

  ```
  <br>	换行
  <hr>    在 HTML 页面中创建水平线
  <!-- 这是一个注释 -->
  <b> 与<i> 定义粗体或斜体文本
  ```

我们暂时可以只了解这些，用到了新的内容是我们将进一步介绍它，学以致用，边学边用一直是我们的目标。

## 一个 HTML 的基础结构组成

![](./HTML_Basic_struct.png)

[示例中的 HTML]() 的基本组成如上所示。

- **<!DOCTYPE html>** 声明为 HTML5 文档
- **<html>** 元素是 HTML 页面的根元素
- **<head>** 元素包含了文档的元（meta）数据，如 **<meta charset="utf-8">** 定义网页编码格式为 **utf-8**。
- **<title>** 元素描述了文档的标题
- **<body>** 元素包含了可见的页面内容
- **<h1>** 元素定义一个大标题
- **<p>** 元素定义一个段落

**注意：**在浏览器的页面上使用键盘上的 F12 按键开启调试模式，就可以看到该网页的组成标签。

查看完整网页声明类型 [DOCTYPE 参考手册](https://www.runoob.com/tags/tag-doctype.html)。

**注意：**对于中文网页需要使用 <meta charset="utf-8"> 声明编码，否则会出现乱码。有些浏览器(如 360 浏览器)会设置 GBK 为默认编码，则你需要设置为 <meta charset="gbk">。

更多HTML 的学习，你可以在网络上收集资料，或者参考 [HTML 教程 | 菜鸟教程 (runoob.com)](https://www.runoob.com/html/html-tutorial.html)。

## 需求及功能解析

本节演示如何在 ESP32 上使用 wifi，并使用 html 文件，编译使用步骤参考：

示例在网页上的显示：

![](./web-2.png)

本系列博客并不是一门专门介绍 HTML 编程的课程，我们只需了解常用的技术就可以了。在 **components/htmls** 目录中还有一些 HTML 文件，你可以将其拷贝到 **components/fs_image** 中并重命名为 index.html，然后重新编译该工程，以查看不同 HTML 文件的效果。下面简单介绍各个 HTML 文件涉及的内容。

## 示例解析

### 目录结构

```
├── CMakeLists.txt
├── main
│   ├── CMakeLists.txt
│   └── main.c                 User application
├── components
│   └── fs_image
		└── index.html
		└── ...
|	└── url_handlers
		└── url_handlers.c
		└── ...
└── README.md                  This is the file you are currently reading
```

- 目录结构主要包含主目录 main，以及组件目录 components. 

- 其中组件目录components中包含了用于存储网页文件的 fs_image 目录（即前述前端文件），以及用于记录 ESP32 上接收来自服务器的请求，并作出响应的 url_handlers 目录（即后端文件）。如前所述，浏览器可以通过 URL 请求服务器端的资源（包括数据和文件），每个 URL 到来时都可以设计一个函数，来决定如何响应该 URL 请求，这便是 url_handlers 要完成的功能。

### 建立前后端代码

1. 为了保存 html 文件以及图片文件到 ESP32 上，url_handlers 目录的 CMakeLists.txt 使用内嵌数据的方式将 fs_image目录的  index.html 和 favicon.ico 文件保存在 ESP32 中：（此外前端代码）

   ```
   idf_component_register(SRCS "url_handlers.c"
                       INCLUDE_DIRS "include"
                       PRIV_REQUIRES esp_http_server
                       EMBED_FILES "../fs_image/favicon.ico" "../fs_image/index.html")
   ```

2. 为了在打开网页时显示 index.html 中的内容，在 url_handlers.c 中实现了 一个处理函数 `index_html_get_handler()`。这部分是后端代码。

   ```
   /* Handler to redirect incoming GET request for /index.html to /
    * This can be overridden by uploading file with same name */
   static esp_err_t index_html_get_handler(httpd_req_t *req)
   {
       extern const char html_start[] asm("_binary_index_html_start");
       extern const char html_end[]   asm("_binary_index_html_end");
       const size_t html_size = (html_end - html_start);
       httpd_resp_set_type(req, "text/html");
       /* Add file upload form and script which on execution sends a POST request to /upload */
       httpd_resp_send_chunk(req, (const char*) html_start, html_size);
       /* Respond with an empty chunk to signal HTTP response completion */
       return httpd_resp_send_chunk(req, NULL, 0);
   }
   ```

3. 在实现了 html 文件以及 对应的 handles 后，可以在 main.c  中注册对应的 handler:

   ```
   static httpd_handle_t start_webserver(void)
   {
       httpd_handle_t server = NULL;
       httpd_config_t config = HTTPD_DEFAULT_CONFIG();
       config.lru_purge_enable = true;
   
       // Start the httpd server
       ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
       if (httpd_start(&server, &config) == ESP_OK) {
           // Set URI handlers
           ESP_LOGI(TAG, "Registering URI handlers");
           for (int i = 0; i < sizeof(httpd_uri_array) / sizeof(httpd_uri_t); i++) {
               if (httpd_register_uri_handler(server, &httpd_uri_array[i]) != ESP_OK) {
                   ESP_LOGE(TAG, "httpd register uri_array[%d] fail", i);
               }
           }
           ESP_LOGI(TAG, "Success starting server!");
           return server;
       }
   
       ESP_LOGI(TAG, "Error starting server!");
       return NULL;
   }
   ```

如此，当打开网页时，浏览器会自动请求名为 index.html 的文件，并显示其中的内容。

### 编译并烧录固件到设备中

1）在工程目录，打开配置菜单

```
idf.py menuconfig
```

主要是配置 wifi 连接的名称和密码：

![](./open-wifi.png)

2）编译烧录固件到设备中

```
idf.py -p PORT build flash monitor
```

(Replace PORT with the name of the serial port to use.)

(To exit the serial monitor, type `Ctrl-]`.)

如果你是新手，请参考 [Getting Started Guide](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html) 搭建编译环境。

3）网页显示

设备烧录固件后，启动该设备，从 log 中查看设备的 IP地址：

```
I (3288) app_wifi: got ip:192.168.47.100
I (3288) esp_netif_handlers: sta ip: 192.168.47.100, mask: 255.255.255.0, gw: 192.168.47.1
I (3288) example_main: Starting server on port: '80'
I (3298) example_main: Registering URI handlers
I (3298) example_main: Success starting server!
```

这里假设设备 IP 地址是 192.168.47.100.

让手机或者电脑与 ESP32 连接同一个路由器，然后打开手机或者电脑上电浏览器，输入上述IP地址，即可打开网页：

![web-1](./web-1.png)

上述示例网页即是本例程 fs_images 目录的 index.html 文件在该浏览器中所程序的样子。

## 讨论

1）输入网址后，浏览器会自动请求 favicon.ico(即上述网页中第一行显示的图标)。

默认情况下，当请求一个网站的 “/” 目录内容时，会默认打开该目录的 index.html 文件。

同样的，默认情况下，浏览器会自动请求 "/"目录下的  favicon.ico 文件，用作网址栏的一个标识图像。

2）如何设计 index.html 文件中的内容，使之在网页上呈现合适的内容？

这正是本系列博客主要介绍的内容。以试验促进理解，在测试实践中学习，敬请参考后续章节。

## 总结

1）本节主要是介绍 HTML 的基础知识。HTML 描述了一个网页中的基本内容；
2）HTML 文档包含了HTML **标签(包含标签和元素，其实他们是一样的)**及**文本内容**。其中 **HTML 标签**是由**尖括号**包围的关键词，HTML 标签通常是成对出现的。
3）在 ESP32 Web 编程中，通过在后端代码中建立一个发送 HTML 内容的 handler 函数，以及在前端代码中实现 HTML 文件来定义网页上的内容。

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c0_01_html_basicstructure) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)
