# ESP32-Web-Server编程-JS 基础 1
## 概述

前述分别在 [HTML 基础](https://blog.csdn.net/wangyx1234/article/details/134630310) 和 [CSS 基础](https://blog.csdn.net/wangyx1234/article/details/133246995) 中介绍了 HTML、CSS 的基本内容。。HTML 定义了网页中包含哪些对象，CSS 定义了对象的显示样式。JavaScript(LiveScript)是一种运行于客户端的解释性脚本语言，使 HTML 页面更具动态性和交互性。**比如实现响应你的点击鼠标、通过输入框向服务器提交登录密码，加载新的网页内容等等交互性功能。**

JavaScript语言与常见的 C 语言、Python 语言类似，是包含顺序语句、逻辑语句的编程语言，当运行指定的语句、函数时就会指定预定义的功能。

JavaScript 主要的作用：

1）能够改变 HTML 内容

2）能够改变 HTML 属性 

3）能够改变 HTML 样式 (CSS) 

4） 能够隐藏 HTML 元素 

5）能够显示 HTML 元素

## JS 的基本语法

JS 是一个定义非常完善的编程语言，与 C\Python 类似，其包含变量(对大小写是敏感的)、语句（语句之间用分号分隔）、函数、作用域、循环结构、选择结构等概念。

**变量示例：**

```
var length = 10;                                  // Number   通过数字字面量赋值 
var points = x * 6;                               // Number   通过表达式字面量赋值
var lastName = "Johnson";                         // String   通过字符串字面量赋值
var cars = ["Sab", "Vlvo", "B0W"];                // Array    通过数组字面量赋值
var person = {firstName:"John", lastName:"Doe"};  // 键值对对象 通过对象字面量赋值
class iot-wang {                                  // 定义一个类
  constructor(name, url) {
    this.name = name;
    this.url = url;
  }
}
```

**函数示例：**

```
// 定义函数，返回 a 乘以 b 的结果
function myFunction(a, b) {
   	return a * b;                           
}
// 调用一次该函数
myFunction();

// 匿名函数
myButton.onclick = function () {
  alert("hello");
};
```

**条件语句：**

```
if (condition1)
{
    当条件 1 为 true 时执行的代码
}
else if (condition2)
{
    当条件 2 为 true 时执行的代码
}
else
{
  当条件 1 和 条件 2 都不为 true 时执行的代码
}
```

**循环语句：**

```
while (i<5)
{
    x=x + "The number is " + i + "<br>";
    i++;
}
```

**事件，当触发一定的事件，如点击鼠标事件时触发执行某函数：**

```
document.querySelector("html").addEventListener("click", () => {
  alert("别戳我，我怕疼。");
});
```

更多 JS 的学习资料可以参考 [JavaScript 教程 | 菜鸟教程 (runoob.com)](https://www.runoob.com/js/js-tutorial.html).

## 在 HTML 中添加 JS 定义

在 THNL 页面中使用 JavaScript 代码主要有两种方法。

- 直接在script标签中嵌入JavaScript代码。

  ```
  <body>
      <script type="text/javaScript">
      	...
  		...
      </script>
  </body>
  ```

  一般在比较大的项目里面是不推荐使用这种方法的，一般都是使用引入外部文件。一来是代码混乱，不好维护代码。二来是代码不美观。

- 引入外部的JavaScript文件。

  ```
  <body>
      <script type="text/javaScript" src="script1.js"></script>
  </body>
  ```
  

进一步地，也可以引入其他网页上的 js 文件：

```
  <body>
      <script type="text/javaScript" 
      		src="http://www.........javaScript.js">
      </script>
  </body
```

JavaScript文件可位于 HTML 的 <body> 或 <head> 部分中，或者同时存在于两个部分中。但是为了整洁，通常都放置在<body> 部分中。并且，通常将 JS 部分放在 HTML 文件的底部附近，因为浏览器会按照代码在文件中的顺序加载 HTML。如果先加载的 JavaScript 期望修改其下方的 HTML，那么它可能由于 HTML 尚未被加载而失效。

## 需求及功能解析

本节主要演示如何在 ESP32 上通过 JS 脚本建立一个实时显示时间的网页。编译并烧录固件到设备参考：

本系列博客并不是一门专门介绍 JS 编程的课程，我们只需边实践边学习，了解常用的技术就可以了。

## 示例解析

与前述的示例类似（建议了解全系列博客-按顺序学习相关内容），本小节主要是 components/fs_image/index.html 中增加 JS 脚本：

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>IOT LAO WANG</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
  </head>
  <body>
    <h1>Date and Time</h1>
    <p id="dateTime"></p>
    // JS 脚本
    <script>
      // var todays_date = new Date();
      // document.getElementById("dateTime").innerHTML = todays_date;
      function time() {
				var vWeek, vWeek_s, vDay;
				vWeek = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
				var date =  new Date();
				year = date.getFullYear();
				month = date.getMonth() + 1;
				day = date.getDate();
				hours = date.getHours();
				minutes = date.getMinutes();
				seconds = date.getSeconds();
				vWeek_s = date.getDay();
				document.getElementById("dateTime").innerHTML = year + "年" + month + "月" + day + "日" + "\t" + hours + ":" + minutes + ":" + seconds + "\t" + vWeek[vWeek_s];
			};
			setInterval("time()", 1000);
    </script>
  </body>
</html>
```

示例效果：

![](./time_show.png)

## 其他 JS 示例

仓库中还提供了其他 JS 示例：

- [js data time extern]()：
- js console

## 讨论

## 总结

1）本节主要是介绍 JavaScript 编程的基础知识，介绍了 JS 编程中的变量、函数、常见语句、事件的基础概念；

2）在 HTML 中添加 JS 定义可以直接在script标签中嵌入JavaScript代码，也可以引入外部的JavaScript文件。

3）示例在 ESP32 Web 中添加了 JS 代码，用于实时显示当前时间。

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c2_00_js_date_time_inter) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)



