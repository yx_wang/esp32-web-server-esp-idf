/* Simple HTTP Server Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include "nvs_flash.h"
#include "esp_netif.h"
#include "driver/gpio.h"

#include <esp_http_server.h>

#include "app_wifi.h"
#include "url_handlers.h"

#define BLINK_GPIO GPIO_NUM_2

/* A simple example that demonstrates how to create GET and POST
 * handlers for the web server.
 */

static const char *TAG = "example_main";    
static httpd_handle_t server = NULL;

/* Handler to redirect incoming GET request for /index.html to /
 * This can be overridden by uploading file with same name */
static void redirect_index_html(httpd_req_t *req)
{
    httpd_resp_set_status(req, "307 Temporary Redirect");
    httpd_resp_set_hdr(req, "Location", "/");
    httpd_resp_send(req, NULL, 0);  // Response body can be empty
}

static esp_err_t light_on_get_handler(httpd_req_t *req)
{
    gpio_set_level(BLINK_GPIO, 1);
    ESP_LOGW(TAG, "light open");
    redirect_index_html(req);
    return ESP_OK;
}

static esp_err_t light_off_get_handler(httpd_req_t *req)
{
    gpio_set_level(BLINK_GPIO, 0);
    ESP_LOGW(TAG, "light close");
    redirect_index_html(req);
    return ESP_OK;
}

httpd_uri_t peri_httpd_uri_array[] = {
    {"/gpio2_on", HTTP_GET, light_on_get_handler, NULL},
    {"/gpio2_off", HTTP_GET, light_off_get_handler, NULL},
};

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        for (int i = 0; i < sizeof(httpd_uri_array) / sizeof(httpd_uri_t); i++) {
            if (httpd_register_uri_handler(server, &httpd_uri_array[i]) != ESP_OK) {
                ESP_LOGE(TAG, "httpd register uri_array[%d] fail", i);
            }
        }

        for (int i = 0; i < sizeof(peri_httpd_uri_array) / sizeof(httpd_uri_t); i++) {
            if (httpd_register_uri_handler(server, &peri_httpd_uri_array[i]) != ESP_OK) {
                ESP_LOGE(TAG, "httpd register peri_uri_array[%d] fail", i);
            }
        }

        ESP_LOGI(TAG, "Success starting server!");
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

static void configure_led(void)
{
    ESP_LOGI(TAG, "Example configured to blink GPIO LED!");
    gpio_reset_pin(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
}

static void stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

void app_main(void)
{
    /* Configure the peripheral according to the LED type */
    configure_led();

    // connect wifi
    app_wifi_main();

    /* Start the server for the first time */
    server = start_webserver();
}
