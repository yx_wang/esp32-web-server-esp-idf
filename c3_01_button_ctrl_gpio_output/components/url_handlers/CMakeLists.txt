# The following five lines of boilerplate have to be in your project's
# CMakeLists in this exact order for cmake to work correctly
cmake_minimum_required(VERSION 3.5)
set(embed_files "../fs_image/favicon.ico" "../fs_image/index.html" "../fs_image/css/stylesheet.css")

idf_component_register(SRCS "url_handlers.c"
                    INCLUDE_DIRS "include"
                    PRIV_REQUIRES esp_http_server
                    EMBED_FILES ${embed_files})