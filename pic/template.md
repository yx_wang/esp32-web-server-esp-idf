# ESP32-Web-Server编程-建立第一个网页

## HTTP 简述

可能你每天都要耍几个短视频，打开几个网页来娱乐一番。当你打开一个网络上的视频或者图片时，其实际发生了下面的流程：

![](./web-http1.PNG)

其中客户端就是你的浏览器啦，服务器就是远程一个存放视频或者图片的电脑啦。他们之间传输数据时需要一个约定的标准，就像两个设备约定共同用一种语言说话一样，目前使用较多的就是 HTTP协议啦。

本系列博客基于乐鑫最流行的开源 ESP-IDF 开发环境。ESP-IDF 中 [HTTP Server](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/protocols/esp_http_server.html) 组件提供了在 ESP32 上运行轻量级 Web 服务器的功能。

## HTTP 的基础的方法

http 1.0定义了3种方法：GET，POST，HEAD

http 1.1新增了6种方法：PUT，DELETE，Options，Trace，Connect，Patch

作为新手，我们主要理解两种方法 GET、POST 方法就可以了。

- GET 方法：GET方法用于使用给定的URI从给定服务器中检索信息，即从指定资源中请求数据。使用GET方法的请求应该只是检索数据，不应对数据产生其他影响。相当与从服务器接收数据。

- POST方法：用于将数据发送到服务器以创建或更新资源。POST请求永远不会被缓存，且对数据长度没有限制；我们无法从浏览器历史记录中查找到POST请求。目前，几乎所有的提交操作都是通过该方法完成。相当于向服务器发送数据。

## HTTP 的 URL、URI

服务器上的文件资源很多，访问哪个资源？用 URI\URL 来指定要访问的资源。

URI = Universal Resource Identifier 统一资源标志符，用来标识抽象或物理资源的一个紧凑字符串。 

URL = Universal Resource Locator 统一资源定位符，一种定位资源的主要访问机制的字符串，一个标准的URL必须包括：protocol、host、port、path、parameter、anchor。 一个典型的 URL 标识：

```
协议://主机[:端口]URI
```

举个栗子先大致理解下他们的含义就可以了，随着学习的深入大家会慢慢理解的更多的。

```
https://www.baidu.com:8080/iot-wang/readme.html?user=xmh&id=1#test
通过解析上述 URL 可以得到各部分的值如下所示。

协议：https。也可以是 ftp、tftp
服务器主机域名：www.baidu.com
服务器端口号：8080
要查询的目标资源路径：/iot-wang/readme.html
客户端传递给服务器的附带参数：? 号开始后的内容，即 user=xmh&id=1，多个参数之间用 & 标识。
目标资源中可能有多个可以访问的资源，比如一个文章有多个段落，指定更详细的段落参数：# 号后的内容，即 test，也可以使用 & 连接多个锚点。
```

## HTTP 的返回码

打开网页时显示 404？使用手机或者电脑的童鞋可能遇到过这种情况。

客户端发起一个请求后，服务器可以返回一个数字代表此次请求是否成功。数字 200 代表此次通信成功，其他数字代表此次通信有点问题：

（1）1xx（提示信息）

 1xx 类状态码属于提示信息，是协议处理中的一种中间状态，实际用到的比较少。

（2）2xx（正确）

2xx 类状态码表示服务器成功处理了客户端的请求，常见的有：

「200 OK」是最常见的成功状态码，表示一切正常。如果是非 HEAD 请求，服务器返回的响应头都会有 body 数据。

「204 No Content」也是常见的成功状态码，与 200 OK 基本相同，但响应头没有 body 数据。 

（3）3xx（重定向）

 3xx 类状态码表示客户端请求的资源发送了变动，需要客户端用新的 URL 重新发送请求获取资源，也就是重定向。

「301 Moved Permanently」表示永久重定向，说明请求的资源已经不存在了，需改用新的 URL 再次访问。

「302 Moved Permanently」表示临时重定向，说明请求的资源还在，但暂时需要用另一个 URL 来访问。

「304 Not Modified」不具有跳转的含义，表示资源未修改，重定向已存在的缓冲文件，也称缓存重定向，用于缓存控制。

301 和 302 都会在响应头里使用字段 Location，指明后续要跳转的 URL，浏览器会自动重定向新的 URL。

（4）4xx（客户端错误）

4xx 类状态码表示客户端发送的报文有误，服务器无法处理，也就是错误码的含义。

「400 Bad Request」表示客户端请求的报文有错误，但只是个笼统的错误。

「403 Forbidden」表示服务器禁止访问资源，并不是客户端的请求出错。

「404 Not Found」表示请求的资源在服务器上不存在或未找到，所以无法提供给客户端。

  [408 Request Timeout]该响应状态码意味着服务器想要关闭这个未使用的连接。

（5）5xx（服务器错误）

 5xx 类状态码表示客户端请求报文正确，但是服务器处理时内部发生了错误，属于服务器端的错误码。

「500 Internal Server Error」与 400 类型，是个笼统通用的错误码，服务器发生了什么错误，我们并不知道。

「501 Not Implemented」表示客户端请求的功能还不支持，类似“即将开业，敬请期待”的意思。

「502 Bad Gateway」通常是服务器作为网关或代理时返回的错误码，表示服务器自身工作正常，访问后端服务器发生了错误。

「503 Service Unavailable」表示服务器当前很忙，暂时无法响应服务器，类似“网络服务正忙，请稍后重试”的意思。

## ESP32 上如何实现网页的发送

### 前端代码

前端代码负责呈现网页上的特效、网页的布局、图片、视频等内容。网站前端工程师的工作内容就是将美工设计的效果图的设计成浏览器可以运行的网页，并和后端开发工程师配合做网页的数据显示和交互。

在本系列课程中前端代码主要是HTML+CSS+JAVASCRIPT。

注意，服务器只是提供前端代码的设计文本，客户端如何解释（即显示）由客户端决定。

### 后端代码

后端代码就是网站的逻辑部分，主要是数据的处理，以及如何响应客户端的请求。比如客户端发起一个不支持的请求，后端代码经过判断会返回一个错误码。

在本系列课程中前端代码主要是C语言代码，感兴趣的也可以了解下可能涉及的数据库。

## 需求及功能解析

本节演示如何在 ESP32 上使用 wifi，并使用一个 html 文件。

## 示例解析

### 目录结构

```
├── CMakeLists.txt
├── main
│   ├── CMakeLists.txt
│   └── main.c                 User application
├── components
│   └── fs_image
		└── index.html
		└── ...
|	└── url_handlers
		└── url_handlers.c
		└── ...
└── README.md                  This is the file you are currently reading
```

- 目录结构主要包含主目录 main，以及组件目录 components. 

- 其中组件目录components中包含了用于存储网页文件的 fs_image 目录（即前述前端文件），以及用于记录 ESP32 上接收来自服务器的请求，并作出响应的 url_handlers 目录（即后端文件）。如前所述，浏览器可以通过 URL 请求服务器端的资源（包括数据和文件），每个 URL 到来时都可以设计一个函数，来决定如何响应该 URL 请求，这便是 url_handlers 要完成的功能。

### 建立前后端代码

1. 为了保存 html 文件以及图片文件到 ESP32 上，url_handlers 目录的 CMakeLists.txt 使用内嵌数据的方式将 fs_image目录的  index.html 和 favicon.ico 文件保存在 ESP32 中：（此外前端代码）

   ```
   idf_component_register(SRCS "url_handlers.c"
                       INCLUDE_DIRS "include"
                       PRIV_REQUIRES esp_http_server
                       EMBED_FILES "../fs_image/favicon.ico" "../fs_image/index.html")
   ```

2. 为了在打开网页时显示 index.html 中的内容，在 url_handlers.c 中实现了 一个处理函数 `index_html_get_handler()`。这部分是后端代码。

   ```
   /* Handler to redirect incoming GET request for /index.html to /
    * This can be overridden by uploading file with same name */
   static esp_err_t index_html_get_handler(httpd_req_t *req)
   {
       extern const char html_start[] asm("_binary_index_html_start");
       extern const char html_end[]   asm("_binary_index_html_end");
       const size_t html_size = (html_end - html_start);
       httpd_resp_set_type(req, "text/html");
       /* Add file upload form and script which on execution sends a POST request to /upload */
       httpd_resp_send_chunk(req, (const char*) html_start, html_size);
       /* Respond with an empty chunk to signal HTTP response completion */
       return httpd_resp_send_chunk(req, NULL, 0);
   }
   ```

3. 在实现了 html 文件以及 对应的 handles 后，可以在 main.c  中注册对应的 handler:

   ```
   static httpd_handle_t start_webserver(void)
   {
       httpd_handle_t server = NULL;
       httpd_config_t config = HTTPD_DEFAULT_CONFIG();
       config.lru_purge_enable = true;
   
       // Start the httpd server
       ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
       if (httpd_start(&server, &config) == ESP_OK) {
           // Set URI handlers
           ESP_LOGI(TAG, "Registering URI handlers");
           for (int i = 0; i < sizeof(httpd_uri_array) / sizeof(httpd_uri_t); i++) {
               if (httpd_register_uri_handler(server, &httpd_uri_array[i]) != ESP_OK) {
                   ESP_LOGE(TAG, "httpd register uri_array[%d] fail", i);
               }
           }
           ESP_LOGI(TAG, "Success starting server!");
           return server;
       }
   
       ESP_LOGI(TAG, "Error starting server!");
       return NULL;
   }
   ```

如此，当打开网页时，浏览器会自动请求名为 index.html 的文件，并显示其中的内容。

### 编译并烧录固件到设备中

1）在工程目录，打开配置菜单

```
idf.py menuconfig
```

主要是配置 wifi 连接的名称和密码：

![](./open-wifi.png)

2）编译烧录固件到设备中

```
idf.py -p PORT build flash monitor
```

(Replace PORT with the name of the serial port to use.)

(To exit the serial monitor, type `Ctrl-]`.)

如果你是新手，请参考 [Getting Started Guide](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html) 搭建编译环境。

3）网页显示

设备烧录固件后，启动该设备，从 log 中查看设备的 IP地址：

```
I (3288) app_wifi: got ip:192.168.47.100
I (3288) esp_netif_handlers: sta ip: 192.168.47.100, mask: 255.255.255.0, gw: 192.168.47.1
I (3288) example_main: Starting server on port: '80'
I (3298) example_main: Registering URI handlers
I (3298) example_main: Success starting server!
```

这里假设设备 IP 地址是 192.168.47.100.

让手机或者电脑与 ESP32 连接同一个路由器，然后打开手机或者电脑上电浏览器，输入上述IP地址，即可打开网页：

![web-1](./web-1.png)

上述示例网页即是本例程 fs_images 目录的 index.html 文件在该浏览器中所程序的样子。

4）功能演示

本节主要是呈现一个网页给用户。

## 讨论

1）输入网址后，浏览器会自动请求 favicon.ico(即上述网页中第一行显示的图标)。

默认情况下，当请求一个网站的 “/” 目录内容时，会默认打开该目录的 index.html 文件。

同样的，默认情况下，浏览器会自动请求 "/"目录下的  favicon.ico 文件，用作网址栏的一个标识图像。

2）如何设计 index.html 文件中的内容，使之在网页上呈现合适的内容？

这正是本系列博客主要介绍的内容。以试验促进理解，在测试实践中学习，敬请参考后续章节。

## 总结

1）本节主要是介绍 HTTP 协议、Web 编程的基础知识，包括HTTP 的基础的方法；什么是 URL、URI；HTTP 的返回码；

2）本节还介绍了 ESP32 上 HTTP 编程的要点，包括如何建立前端、后端代码，它们如何相互作用以实现一个 Web 应用。

## 资源链接

1）[Learning-FreeRTOS-with-esp32 系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/127201095?spm=1001.2014.3001.5501)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/learn_freertos_with_esp32/tree/master/esp32-samples/C2_01_resource_sharing_between_tasks_Critical) （点击直达代码仓库）

3）下一篇：[RTOS共享资源保护-关调度实现共享资源的保护](https://blog.csdn.net/wangyx1234/article/details/128058005)

(码字不易感谢点赞或收藏)
