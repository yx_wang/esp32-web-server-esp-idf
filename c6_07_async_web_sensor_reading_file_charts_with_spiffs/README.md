# 从文件中读取数据并显示

https://randomnerdtutorials.com/esp8266-nodemcu-date-time-ntp-client-server-arduino/

https://randomnerdtutorials.com/esp8266-nodemcu-plot-readings-charts-multiple/

https://randomnerdtutorials.com/esp32-esp8266-thermostat-web-server/



该示例应该是必须使用异步 web 的，因为一旦进入到 ssh 的 new_readings 的 handle 中，http server 进程进入了 while() 了，次时无法响应其他的请求了。

暂缓该篇开发，了解 master 分支的 async web 后再开发或者退而求其次，更改其实现逻辑，比如只是用文件定时记录信息，然后在网页端做历史数据的可视化。

结合文件服务器示例，在文件服务器上下载文件（新建一个 URL，这个 URL 完成提供下载文件的服务，后期还可以结合数据库，允许下载数据库文件）。

通过 Ajax 更新最新的几条记录。还是单线程网页。

这个可以做成较复杂的案例，因为 master 上的 async 示例已经可以使用了，融合两个示例就可以作出一个可以实时显示并提供文件下载服务的示例。

不用提供完整的文件服务器的支持，提供单个文件的下载支持就可以了。用户可以自己实现多网页，来实现这个功能。

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c0_00_sample_open_html) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)
