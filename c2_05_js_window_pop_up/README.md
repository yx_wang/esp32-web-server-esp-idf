# ESP32-Web-Server编程- JS 基础 6

## 概述

演示在网页上通过 `window.alert()`弹出一个弹窗。

```
<p><button onclick="trige_waring()">Don't clike me</button></p>
<script>
  function trige_waring() {
    var a = "Oh, This is a waring";
    window.alert(a);
  }
</script>
```

## 示例效果

![](./windows_pop_test.png)

点击按钮：

![](./windows_pop_warning.png)

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c2_05_js_window_pop_up) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)
