# ESP32-Web-Server编程-JS 基础 2

## 概述

上节介绍了 JS 编程的基础。如前所述，在 HTML 中，可以通过下述 两种方式使用 JS 程序：

- 直接在 HTML 文件中通过 script 标签中**嵌入 JavaScript 代码**。
- 通过 src 元素引入**外部的 JavaScript 文件**。

在[上一个小节](https://blog.csdn.net/wangyx1234/article/details/134639355)的示例中，我们介绍了第一种方式。本小节主要介绍第二节方式。

## 示例解析

相比上节的示例，本小节主要添加了三处更改：

1）在 index.html 中通过 `script`指定需要的  **JavaScript 文件**

```
<body>
    <h1>Date and Time</h1>
    <p id="dateTime"></p>
    <script src="script.js"></script>
  </body>
```

2）在 components/fs_image/js 目录新建 script.js 文件：

```
function time() {
    var vWeek, vWeek_s, vDay;
    vWeek = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    var date =  new Date();
    year = date.getFullYear();
    month = date.getMonth() + 1;
    day = date.getDate();
    hours = date.getHours();
    minutes = date.getMinutes();
    seconds = date.getSeconds();
    vWeek_s = date.getDay();
    document.getElementById("dateTime").innerHTML = year + "年" + month + "月" + day + "日" + "\t" + hours + ":" + minutes + ":" + seconds + "\t" + vWeek[vWeek_s];
};
setInterval("time()", 1000);
```

3）更改后端代码，增加请求 JS 文件的 hander:

```
/* Handler to respond with js.
 * Browsers expect to GET website icon at URI /stylesheet.css.*/
static esp_err_t js_get_handler(httpd_req_t *req)
{
    extern const unsigned char js_start[] asm("_binary_script_js_start");
    extern const unsigned char js_end[]   asm("_binary_script_js_end");
    const size_t js_size = (js_end - js_start);
    httpd_resp_set_type(req, "text/javascript");
    httpd_resp_send(req, (const char *)js_start, js_size);
    return ESP_OK;
}
```

与前面提到的类似，在加载 HTML 文件时，若发现 HTML 通过 **src** 指定了要加载的文件，浏览器会自动发起 Get 请求来向服务器请求对应的资源。

## 总结

1）本节主要是介绍引入外部 JavaScript 文件的方法。与前述在 HTML 中引入外部 CSS 文件类似，引入外部 JS 文件也需要同时更改前端、后端代码。

2）示例在 ESP32 Web 中添加了 JS 代码，用于实时显示当前时间。

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c2_01_js_date_time_extern) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)
