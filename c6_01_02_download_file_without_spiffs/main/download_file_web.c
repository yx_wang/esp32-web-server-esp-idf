// Copyright 2020-2022 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

#include "esp_http_server.h"
#include "sdkconfig.h"
#include "esp_log.h"

static httpd_handle_t pic_httpd = NULL;

static const char *TAG = "bin_f";

/* Handler to download a file kept on the server */
static esp_err_t bin_get_handler(httpd_req_t *req)
{
    esp_err_t res = ESP_OK;
    char data_buf[] = {0x00, 0x01, 0x02, 0x03};

    httpd_resp_set_type(req, "application/octet-stream");
    httpd_resp_set_hdr(req, "Content-Disposition", "inline; filename=record.bin"); // default name is record.bin
    httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");

    if (res == ESP_OK) {
        res = httpd_resp_send_chunk(req, (const char *)data_buf, sizeof(data_buf)/sizeof(char));
    } else {
        ESP_LOGW(TAG, "exit file server");
        return ESP_FAIL;
    }
    /* Respond with an empty chunk to signal HTTP response completion */
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

esp_err_t start_file_server()
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.stack_size = 5120;

    httpd_uri_t record_file_uri = {
        .uri = "/record",
        .method = HTTP_GET,
        .handler = bin_get_handler,
        .user_ctx = NULL
    };

    ESP_LOGI(TAG, "Starting file server without file systemon, port: '%d'", config.server_port);
    if (httpd_start(&pic_httpd, &config) == ESP_OK) {
        httpd_register_uri_handler(pic_httpd, &record_file_uri);
        return ESP_OK;
    }
    return ESP_FAIL;
}