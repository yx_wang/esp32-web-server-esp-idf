

# ESP32-Web-Server编程- JS 基础 7

## 概述

console.log() 方法用于在控制台输出信息。

该方法对于开发过程进行测试很有帮助。

**提示:** 在测试该方法的过程中，控制台需要可见 (浏览器按下 F12 打开控制台)。

## 示例解析

### 前端代码

在前端代码的 js 脚本中，调用 `console.log` 方法。当运行到该函数时，将在控制台中输出对应的 log 提示。

```
<script>
  var count = 0;
  function trige_waring() {
    var myObj = { name : "LaoWang", say : "Ohh, you click me" };
    console.log(myObj);
    console.log("click_count=", count);
    count = count + 1;
  }
</script>
```

F12按下后，每次点击按钮，控制台出现 console 提示：

全局变量 click 记录按下的次数。并打印固定的 Json 对象：

![](./click_F12_see_console.png)

## 总结

1）本节主要演示使用 console 帮助调试前端程序。

2）使用 console 需要打开浏览器控制台，可以通过 F12 快捷键打开控制台程序。

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c2_06_js_console) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)

