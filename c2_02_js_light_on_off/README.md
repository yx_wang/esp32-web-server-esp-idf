# ESP32-Web-Server编程- JS 基础 3

## 概述

本示例演示通过 button 控件的 onclick 内联属性，实现在网页上点击按钮，切换 LED 灯图标的转变。

## 示例解析

### 前端设计

前端代码建立了一个 id 为 "imageLamp" 的图片对象。并建立两个按钮，设计两个按钮的 onclick 事件为引用不同的图片。

```
<p>
  <img id="imageLamp" src="light_on.png">
</p>
<p>
  <button onclick="document.getElementById('imageLamp').src='light_on.png'">Turn on the light</button>
</p>
<p>
  <button onclick="document.getElementById('imageLamp').src='light_off.png'">Turn off the light</button>
</p>
```

### 后端设计

前端在加载图片时会自动向 web 服务器发起请求图片的 get 请求，因此后端代码增加了发送两个图片的 handler:

```
{"/light_on.png", HTTP_GET, light_on_get_handler, NULL},
{"/light_off.png", HTTP_GET, light_off_get_handler, NULL},
```

### 示例效果

点击不同的按钮可以切换网页的显示效果：

![](./light_control.png)

## 总结

1）本节主要是演示最常见的控件-button，以及可以绑定一个事件来描述按钮按下时发生的行为。

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c2_02_js_light_on_off) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)
