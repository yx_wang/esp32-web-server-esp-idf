// Copyright 2020 Espressif Systems (Shanghai) Co. Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#ifndef _URL_HANDLERS_H_
#define _URL_HANDLERS_H_

#include <esp_http_server.h>

#define URL_HANDLERS_MAX (5)

#ifdef __cplusplus
extern "C" {
#endif

extern httpd_uri_t httpd_uri_array[URL_HANDLERS_MAX];

#ifdef __cplusplus
}
#endif

#endif
