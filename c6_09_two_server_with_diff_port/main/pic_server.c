// Copyright 2020-2022 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

#include "esp_http_server.h"
#include "sdkconfig.h"
#include "esp_log.h"

static httpd_handle_t pic_httpd = NULL;

static const char *TAG = "pic_s";

#define CONFIG_IMAGE_JPEG_FORMAT (1)

/* Handler to download a file kept on the server */
static esp_err_t pic_get_handler(httpd_req_t *req)
{
    esp_err_t res = ESP_OK;
    static uint32_t pic_index = 0;
    size_t image_data_buf_len = 0;
    uint8_t *image_data_buf = NULL;
    extern const unsigned char pic1_jpg_start[] asm("_binary_pic1_jpg_start");
    extern const unsigned char pic1_jpg_end[]   asm("_binary_pic1_jpg_end");
    extern const unsigned char pic2_jpg_start[] asm("_binary_pic2_jpg_start");
    extern const unsigned char pic2_jpg_end[]   asm("_binary_pic2_jpg_end");

#if CONFIG_IMAGE_JPEG_FORMAT
    httpd_resp_set_type(req, "image/jpg");
    httpd_resp_set_hdr(req, "Content-Disposition", "inline; filename=capture.jpg");
#elif CONFIG_IMAGE_BMP_FORMAT
    httpd_resp_set_type(req, "image/bmp");
    httpd_resp_set_hdr(req, "Content-Disposition", "inline; filename=capture.bmp");
#endif
    httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");

    switch (pic_index) {
    case 0:
        image_data_buf_len = (pic1_jpg_end - pic1_jpg_start);
        image_data_buf = pic1_jpg_start;
        pic_index = 1;
        break;
    case 1:
        image_data_buf_len = (pic2_jpg_end - pic2_jpg_start);
        image_data_buf = pic2_jpg_start;
        pic_index = 0;
        break;
    default:
        break;
    }

    if (res == ESP_OK) {
        res = httpd_resp_send_chunk(req, (const char *)image_data_buf, image_data_buf_len);
        ESP_LOGI(TAG, "pic len %d", image_data_buf_len);
    } else {
        ESP_LOGW(TAG, "exit pic server");
        return ESP_FAIL;
    }
    /* Respond with an empty chunk to signal HTTP response completion */
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

esp_err_t start_pic_server(int index)
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.stack_size = 5120;
    config.server_port += index;
    config.ctrl_port += index;
    
    httpd_uri_t pic_uri = {
        .uri = "/pic",
        .method = HTTP_GET,
        .handler = pic_get_handler,
        .user_ctx = NULL
    };

    ESP_LOGI(TAG, "Starting pic server on port: '%d'", config.server_port);
    if (httpd_start(&pic_httpd, &config) == ESP_OK) {
        httpd_register_uri_handler(pic_httpd, &pic_uri);
        return ESP_OK;
    }
    return ESP_FAIL;
}