# ESP32-Web-Server编程- JS 基础 8

## 概述

 本节演示通过鼠标的事件机制，在网页上显示不同的效果。

## 示例解析

### 前端设计

前端代码建立了两断 text 文字，并设置了鼠标按下-mouseDown()、鼠标抬起-mouseUp()，鼠标在文字上-mouseOver()、鼠标离开文字-mouseOut() 四个事件。

```
<body>
 	<p id="text1" onmousedown="mouseDown()" onmouseup="mouseUp()">Press down to change color to red. Release to change to green.</p>
    <p id="text2" onmouseover="mouseOver()" onmouseout="mouseOut()">Move the mouse over this text to make it bigger.</p>
    <script>
      function mouseDown() {
        document.getElementById("text1").style.color = "red";
      }
      function mouseUp() {
        document.getElementById("text1").style.color = "green";
      }
      function mouseOver() {
        document.getElementById("text2").style.fontSize = "20px";
      }
      function mouseOut() {
        document.getElementById("text2").style.fontSize = "16px";
      }
    </script>
  </body>
```

### 示例效果

移动鼠标在不同的文字上，可以看到不同的效果：

![](./mouse_event.png)

## 总结

1）本节主要是演示鼠标事件的使用方法。在网页端可以设计鼠标按下-mouseDown()、鼠标抬起-mouseUp()，鼠标在文字上-mouseOver()、鼠标离开文字-mouseOut() 四个事件。

## 资源链接

1）[ESP32-Web-Server ESP-IDF系列博客介绍](https://blog.csdn.net/wangyx1234/article/details/133246944)
2）对应示例的 [code 链接](https://gitee.com/yx_wang/esp32-web-server-esp-idf/tree/master/c2_07_js_events) （点击直达代码仓库）

3）下一篇：

(码字不易感谢点赞或收藏)
