/* Simple HTTP Server Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdlib.h>
#include <stdint.h>

#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include "nvs_flash.h"
#include "esp_vfs_semihost.h"
#include "esp_vfs_fat.h"
#include "esp_spiffs.h"

#include "esp_netif.h"
#include "driver/gpio.h"
#include "cJSON.h"

#include <esp_http_server.h>

#include "app_wifi.h"

// Set number of outputs
#define NUM_OUTPUTS  4

#define FILE_PATH_MAX (ESP_VFS_PATH_MAX + 128)
#define SCRATCH_BUFSIZE (10240)

#define REST_CHECK(a, str, goto_tag, ...)                                              \
    do                                                                                 \
    {                                                                                  \
        if (!(a))                                                                      \
        {                                                                              \
            ESP_LOGE(TAG, "%s(%d): " str, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
            goto goto_tag;                                                             \
        }                                                                              \
    } while (0)

typedef struct rest_server_context {
    char base_path[ESP_VFS_PATH_MAX + 1];
    char scratch[SCRATCH_BUFSIZE];
} rest_server_context_t;

#define CHECK_FILE_EXTENSION(filename, ext) (strcasecmp(&filename[strlen(filename) - strlen(ext)], ext) == 0)
#define TEMP_STR_MAX_BYTE_SIZE (128)


// Html return Parameters
static const char *param_output = "output";
static const char *param_state = "state";

// Array with outputs you want to control
int outputGPIOs[NUM_OUTPUTS] = {2, 4, 12, 14};

/* A simple example that demonstrates how to create GET and POST
 * handlers for the web server.
 */

static const char *TAG = "example_main";    
static httpd_handle_t server = NULL;

//Stupid li'l helper function that returns the value of a hex char.
static int httpdHexVal(char c)
{
    if (c >= '0' && c <= '9') {
        return c - '0';
    }

    if (c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    }

    if (c >= 'a' && c <= 'f') {
        return c - 'a' + 10;
    }

    return 0;
}

//Decode a percent-encoded value.
//Takes the valLen bytes stored in val, and converts it into at most retLen bytes that
//are stored in the ret buffer. Returns the actual amount of bytes used in ret. Also
//zero-terminates the ret buffer.
int httpdUrlDecode(char* val, int valLen, char* ret, int retLen)
{
    int s = 0, d = 0;
    int esced = 0, escVal = 0;

    while (s < valLen && d < retLen) {
        if (esced == 1)  {
            escVal = httpdHexVal(val[s]) << 4;
            esced = 2;
        } else if (esced == 2) {
            escVal += httpdHexVal(val[s]);
            ret[d++] = escVal;
            esced = 0;
        } else if (val[s] == '%') {
            esced = 1;
        } else if (val[s] == '+') {
            ret[d++] = ' ';
        } else {
            ret[d++] = val[s];
        }

        s++;
    }

    if (d < retLen) {
        ret[d] = 0;
    }

    return d;
}

//Find a specific arg in a string(/update?output=4&state=1) of get- or post-data.
//Line is the string of post/get-data, arg is the name of the value to find. The
//zero-terminated result is written in buff, with at most buffLen bytes used. The
//function returns the length of the result, or -1 if the value wasn't found. The
//returned string will be urldecoded already.
int httpd_find_arg(const char* line, const char* arg, char* buff, int buffLen)
{
    char* p, *e;
    bool first_in = true;

    if (line == NULL) {
        return -1;
    }

    p = line;

    while (p != NULL && *p != '\n' && *p != '\r' && *p != 0) {
        // printf("findArg: %s\n", p);
        if (strncmp(p, arg, strlen(arg)) == 0 && p[strlen(arg)] == '=') {
            p += strlen(arg) + 1; //move p to start of value
            e = (char*)strstr(p, "&");

            if (e == NULL) {
                e = p + strlen(p);
            }

            // printf("findArg: val %s len %d\n", p, (e-p));
            return httpdUrlDecode(p, (e - p), buff, buffLen);
        }

        if(first_in) {
            p = (char*)strstr(p, "?");
            first_in = false;
        } else {
            p = (char*)strstr(p, "&");
        }
        
        if (p != NULL) {
            p += 1;
        }
    }

    printf("Finding %s in %s: Not found :/\n", arg, line);
    return -1; //not found
}

/* Set HTTP response content type according to file extension */
static esp_err_t set_content_type_from_file(httpd_req_t* req, const char* filepath)
{
    const char* type = "text/plain";
    if (CHECK_FILE_EXTENSION(filepath, ".html")) {
        type = "text/html";
    } else if (CHECK_FILE_EXTENSION(filepath, ".js")) {
        type = "application/javascript";
    } else if (CHECK_FILE_EXTENSION(filepath, ".css")) {
        type = "text/css";
    } else if (CHECK_FILE_EXTENSION(filepath, ".png")) {
        type = "image/png";
    } else if (CHECK_FILE_EXTENSION(filepath, ".ico")) {
        type = "image/x-icon";
    } else if (CHECK_FILE_EXTENSION(filepath, ".svg")) {
        type = "text/xml";
    }
    return httpd_resp_set_type(req, type);
}

/* Send HTTP response with the contents of the requested file */
static esp_err_t rest_common_get_handler(httpd_req_t* req)
{
    char filepath[FILE_PATH_MAX];
    rest_server_context_t* rest_context = (rest_server_context_t*) req->user_ctx;
    strlcpy(filepath, rest_context->base_path, sizeof(filepath));
    if (req->uri[strlen(req->uri) - 1] == '/') {
        strlcat(filepath, "/index.html", sizeof(filepath));
    } else {
        strlcat(filepath, req->uri, sizeof(filepath));
    }

    char* p = strrchr(filepath, '?');
    if (p != NULL) {
        *p = '\0';
    }

    int fd = open(filepath, O_RDONLY, 0);
    if (fd == -1) {
        ESP_LOGE(TAG, "Failed to open file : %s", filepath);
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to read existing file");
        return ESP_FAIL;
    }

    set_content_type_from_file(req, filepath);

    char* chunk = rest_context->scratch;
    ssize_t read_bytes;
    do {
        /* Read file in chunks into the scratch buffer */
        read_bytes = read(fd, chunk, SCRATCH_BUFSIZE);
        if (read_bytes == -1) {
            ESP_LOGE(TAG, "Failed to read file : %s", filepath);
        } else if (read_bytes > 0) {
            /* Send the buffer contents as HTTP response chunk */
            if (httpd_resp_send_chunk(req, chunk, read_bytes) != ESP_OK) {
                close(fd);
                ESP_LOGE(TAG, "File sending failed!");
                /* Abort sending file */
                httpd_resp_sendstr_chunk(req, NULL);
                /* Respond with 500 Internal Server Error */
                httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to send file");
                return ESP_FAIL;
            }
        }
    } while (read_bytes > 0);
    /* Close file after sending complete */
    close(fd);
    ESP_LOGI(TAG, "File sending complete");
    /* Respond with an empty chunk to signal HTTP response completion */
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

/* A help function to get post request data */
static esp_err_t recv_post_data(httpd_req_t* req, char* buf)
{
    int total_len = req->content_len;
    int cur_len = 0;
    int received = 0;

    if (total_len >= SCRATCH_BUFSIZE) {
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "content too long");
        return ESP_FAIL;
    }
    while (cur_len < total_len) {
        received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to post control value");
            return ESP_FAIL;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';//now ,the post is str format, like ssid=yuxin&pwd=TestPWD&chl=1&ecn=0&maxconn=1&ssidhidden=0
    ESP_LOGI(TAG, "Post data is : %s\n", buf);
    return ESP_OK;
}

// void m_web_response_OK(httpd_req_t *req)
// {
//     httpd_resp_set_type(req, HTTPD_TYPE_JSON);
//     httpd_resp_set_status(req, HTTPD_200);
//     httpd_resp_sendstr(req, "{\"state\": 0}");
// }

// void m_web_response_error(httpd_req_t *req, const char *status)
// {
//     httpd_resp_set_type(req, HTTPD_TYPE_JSON);
//     httpd_resp_set_status(req, status);
//     httpd_resp_sendstr(req, "{\"state\": 1}");
// }

/* Handler to redirect incoming GET request for /index.html to /
 * This can be overridden by uploading file with same name */
static void redirect_index_html(httpd_req_t *req)
{
    httpd_resp_set_status(req, "307 Temporary Redirect");
    httpd_resp_set_hdr(req, "Location", "/");
    httpd_resp_send(req, NULL, 0);  // Response body can be empty
}

static const char web_base_point[] = "/www";

esp_err_t init_fs(void)
{
    esp_vfs_spiffs_conf_t conf = {
        .base_path = web_base_point,
        .partition_label = NULL,
        .max_files = 5,//maybe the num can be set smaller
        .format_if_mount_failed = false
    };
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return ESP_FAIL;
    }

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
    } else {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }
    return ESP_OK;
}

// Return JSON with Current States
char *getSeneorStates() 
{
    char temp_str1[32] = {0};
    char temp_str2[32] = {0};
    char temp_str3[32] = {0};
    cJSON* root = cJSON_CreateObject();

    // Give some num to simulate the valuse of the sensor
    static uint32_t temp_num = 0;
    static uint32_t hum_num = 10;
    static uint32_t pre_num = 20;
    temp_num++;
    hum_num++;
    pre_num++;

    itoa(temp_num, temp_str1, 10);
    cJSON_AddStringToObject(root, "temperature", temp_str1);
    itoa(hum_num, temp_str2, 10);
    cJSON_AddStringToObject(root, "humidity", temp_str2);
    itoa(pre_num, temp_str3, 10);
    cJSON_AddStringToObject(root, "pressure", temp_str3);

    char* printed_json = cJSON_Print(root);
    cJSON_Delete(root);

    return printed_json;
}

static esp_err_t readings_get_handler(httpd_req_t *req)
{
    ESP_LOGI(TAG, "req:%s", req->uri);
    char* printed_json = getSeneorStates();
    ESP_LOGI(TAG, "reading sensor info:%s\n", printed_json);
    httpd_resp_set_type(req, "application/json");
    httpd_resp_set_status(req, HTTPD_200);

    httpd_resp_sendstr(req, printed_json);
    free((void*) printed_json);

    return ESP_OK;
}

#define TEMP_MESSAGE_MAX_LEN (1024)
static char temp_message[TEMP_MESSAGE_MAX_LEN];

static char *generateEventMessage(const char *message, const char *event, uint32_t id, uint32_t reconnect, int *ret_body_len) 
{
    char temp_str[32] = {0};
    int body_len1 = 0;

    memset(temp_message, '\0', TEMP_MESSAGE_MAX_LEN);

    if(reconnect){
        body_len1 += sprintf((char *)temp_message + body_len1, "retry: ");
        itoa(reconnect, temp_str, 10);
        body_len1 += sprintf((char *)temp_message + body_len1, temp_str);
        body_len1 += sprintf((char *)temp_message + body_len1, "\r\n");
        memset(temp_str, '\0', 32);
    }

    if(id){
        body_len1 += sprintf((char *)temp_message + body_len1, "id: ");
        itoa(id, temp_str, 10);
        body_len1 += sprintf((char *)temp_message + body_len1, temp_str);
        body_len1 += sprintf((char *)temp_message + body_len1, "\r\n");
        memset(temp_str, '\0', 32);
    }

    if(event != NULL){
        body_len1 += sprintf((char *)temp_message + body_len1, "event: ");
        itoa(id, temp_str, 10);
        body_len1 += sprintf((char *)temp_message + body_len1, event);
        body_len1 += sprintf((char *)temp_message + body_len1, "\r\n");
        memset(temp_str, '\0', 32);
    }

    if(message != NULL){
    size_t messageLen = strlen(message);
    char * lineStart = (char *)message;
    char * lineEnd;
    do {
        char * nextN = strchr(lineStart, '\n');
        char * nextR = strchr(lineStart, '\r');
        if(nextN == NULL && nextR == NULL){
        size_t llen = ((char *)message + messageLen) - lineStart;
        char * ldata = (char *)malloc(llen+1);
        if(ldata != NULL){
            memcpy(ldata, lineStart, llen);
            ldata[llen] = 0;
            body_len1 += sprintf((char *)temp_message + body_len1, "data: ");
            body_len1 += sprintf((char *)temp_message + body_len1, ldata);
            body_len1 += sprintf((char *)temp_message + body_len1, "\r\n\r\n");
            free(ldata);
        }
        lineStart = (char *)message + messageLen;
        } else {
        char * nextLine = NULL;
        if(nextN != NULL && nextR != NULL){
            if(nextR < nextN){
            lineEnd = nextR;
            if(nextN == (nextR + 1))
                nextLine = nextN + 1;
            else
                nextLine = nextR + 1;
            } else {
            lineEnd = nextN;
            if(nextR == (nextN + 1))
                nextLine = nextR + 1;
            else
                nextLine = nextN + 1;
            }
        } else if(nextN != NULL){
            lineEnd = nextN;
            nextLine = nextN + 1;
        } else {
            lineEnd = nextR;
            nextLine = nextR + 1;
        }

        size_t llen = lineEnd - lineStart;
        char * ldata = (char *)malloc(llen+1);
        if(ldata != NULL){
            memcpy(ldata, lineStart, llen);
            ldata[llen] = 0;
            body_len1 += sprintf((char *)temp_message + body_len1, "data: ");
            body_len1 += sprintf((char *)temp_message + body_len1, ldata);
            body_len1 += sprintf((char *)temp_message + body_len1, "\r\n");
            free(ldata);
        }
        lineStart = nextLine;
        if(lineStart == ((char *)message + messageLen))
            body_len1 += sprintf((char *)temp_message + body_len1, "\r\n");
        }
    } while(lineStart < ((char *)message + messageLen));
    }

    *ret_body_len = body_len1;

    return temp_message;
}

static esp_err_t events_get_handler(httpd_req_t *req)
{
    ESP_LOGI(TAG, "req:%s", req->uri);
    esp_err_t res = ESP_OK;
    int msg_len = 0;
    res = httpd_resp_set_type(req, "text/event-stream");
    if (res != ESP_OK) {
        return res;
    }

    httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");
    httpd_resp_set_hdr(req, "Cache-Control", "no-cache");
    httpd_resp_set_hdr(req, "Connection","keep-alive");
    httpd_resp_set_status(req, HTTPD_200);
    uint64_t total_time = esp_timer_get_time();

    while (true) {
        // if (res == ESP_OK) {
        //     char* printed_json = getSeneorStates();
        //     ESP_LOGI(TAG, "get sensor info:%s\n", printed_json);
        //     char *msg = generateEventMessage((const char *)printed_json, "new_readings", 1, 0, &msg_len);
        //     res = httpd_resp_send_chunk(req, (const char *)msg, msg_len);
        //     vTaskDelay(1000/portTICK_RATE_MS);
        // }
        // get sensor info every 1000000 us
        if ((esp_timer_get_time() - total_time) > 1000000) {
            char* printed_json = getSeneorStates();
            ESP_LOGI(TAG, "get sensor info:%s\n", printed_json);
            char *msg = generateEventMessage((const char *)printed_json, "new_readings", 1, 0, &msg_len);
            res = httpd_resp_send_chunk(req, (const char *)msg, msg_len);
            vTaskDelay(1000/portTICK_RATE_MS);
            total_time = esp_timer_get_time();
        }
    }

    return ESP_OK;
}

esp_err_t start_webserver(const char* base_path)
{
    REST_CHECK(base_path, "wrong base path", err);
    rest_server_context_t* rest_context = calloc(1, sizeof(rest_server_context_t));
    REST_CHECK(rest_context, "No memory for rest context", err);
    strlcpy(rest_context->base_path, base_path, sizeof(rest_context->base_path));

    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.max_uri_handlers = 7;
    config.max_open_sockets = 7;
    config.uri_match_fn = httpd_uri_match_wildcard;

    ESP_LOGI(TAG, "Starting HTTP Server");
    REST_CHECK(httpd_start(&server, &config) == ESP_OK, "Start server failed", err_start);

    httpd_uri_t httpd_uri_array[] = {
        {"/readings", HTTP_GET, readings_get_handler, rest_context},
        {"/events", HTTP_GET, events_get_handler, rest_context},
        {"/*", HTTP_GET, rest_common_get_handler,rest_context}//Catch-all callback function for the filesystem, this must be set to the array last one
    };

    for(int i = 0; i < sizeof(httpd_uri_array)/sizeof(httpd_uri_t); i++){
        if (httpd_register_uri_handler(server, &httpd_uri_array[i]) != ESP_OK) {
            ESP_LOGE(TAG, "httpd register uri_array[%d] fail", i);
        }
    }

    return ESP_OK;
err_start:
    free(rest_context);
err:
    return ESP_FAIL;
}

static void stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

void app_main(void)
{
    // connect wifi
    app_wifi_main();

    /* Start the server for the first time */
    ESP_ERROR_CHECK(init_fs());
    server = start_webserver(web_base_point);
}
