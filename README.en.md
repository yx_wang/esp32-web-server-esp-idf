# ESP32-Web-Server-ESP-IDF

#### Description
Build a HTTP Web server or WebSocket Web server on ESP32 using ESP-IDF.

#### Software Architecture
Software architecture description

#### Instructions

1.  Based on the ESP-IDF development environment.
2.  The main programming language is C.
3.  It is divided into three parts: basic section, improvement section, and project section.
4.  Covers HTML, CSS, JavaScript and client-server communication protocols, for example, Websocket and HTTP protocols.
5.  Some important web technologies, such as SSE(Server-Sent Events), AJAX(Asynchronous JavaScript and XML).
6.  Some typical IoT applications, such as distribution network through the web, OTA through the web, logging into a webpage through the web, and transferring files through the web, etc.

### Hardware Required

* A development board with ESP32/ESP32-S2/ESP32-C3 SoC (e.g., ESP32-DevKitC, ESP-WROVER-KIT, etc.)
* A USB cable for power supply and programming

### Build Software Development Environment

Refer to [Get-Started](https://docs.espressif.com/projects/esp-idf/en/release-v4.4/esp32/get-started/index.html) in ESP-IDF.

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request

