#pragma once
#include <esp_http_server.h>

esp_err_t OTA_update_post_handler(httpd_req_t *req);
esp_err_t OTA_update_status_handler(httpd_req_t *req);
